﻿namespace PluginManager
{
    partial class PluginManagerForm
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PluginManagerForm));
            button1 = new Button();
            pluginsBox = new CheckedListBox();
            sortUp = new Button();
            sortDown = new Button();
            selectAllButton = new Button();
            button3 = new Button();
            newDirButton = new Button();
            SuspendLayout();
            // 
            // button1
            // 
            button1.BackColor = SystemColors.ControlDarkDark;
            button1.FlatAppearance.BorderColor = Color.DarkGray;
            button1.FlatStyle = FlatStyle.Flat;
            button1.Location = new Point(330, 604);
            button1.Name = "button1";
            button1.Size = new Size(113, 23);
            button1.TabIndex = 0;
            button1.Text = "Save";
            button1.UseVisualStyleBackColor = false;
            button1.Click += button1_Click;
            // 
            // pluginsBox
            // 
            pluginsBox.BackColor = SystemColors.ControlDark;
            pluginsBox.BorderStyle = BorderStyle.FixedSingle;
            pluginsBox.FormattingEnabled = true;
            pluginsBox.ImeMode = ImeMode.NoControl;
            pluginsBox.Location = new Point(26, 53);
            pluginsBox.Name = "pluginsBox";
            pluginsBox.Size = new Size(387, 524);
            pluginsBox.TabIndex = 1;
            // 
            // sortUp
            // 
            sortUp.FlatAppearance.BorderColor = Color.DarkGray;
            sortUp.FlatStyle = FlatStyle.Flat;
            sortUp.Location = new Point(419, 53);
            sortUp.Name = "sortUp";
            sortUp.Size = new Size(24, 23);
            sortUp.TabIndex = 2;
            sortUp.Text = "+";
            sortUp.UseVisualStyleBackColor = true;
            sortUp.Click += sortUp_Click;
            // 
            // sortDown
            // 
            sortDown.FlatAppearance.BorderColor = Color.DarkGray;
            sortDown.FlatStyle = FlatStyle.Flat;
            sortDown.Location = new Point(419, 82);
            sortDown.Name = "sortDown";
            sortDown.Size = new Size(24, 23);
            sortDown.TabIndex = 3;
            sortDown.Text = "-";
            sortDown.UseVisualStyleBackColor = true;
            sortDown.Click += sortDown_Click;
            // 
            // selectAllButton
            // 
            selectAllButton.FlatAppearance.BorderColor = Color.DarkGray;
            selectAllButton.FlatStyle = FlatStyle.Flat;
            selectAllButton.Location = new Point(26, 12);
            selectAllButton.Name = "selectAllButton";
            selectAllButton.Size = new Size(75, 23);
            selectAllButton.TabIndex = 4;
            selectAllButton.Text = "Select All";
            selectAllButton.UseVisualStyleBackColor = true;
            selectAllButton.Click += selectAllButton_Click;
            // 
            // button3
            // 
            button3.FlatAppearance.BorderColor = Color.DarkGray;
            button3.FlatStyle = FlatStyle.Flat;
            button3.Location = new Point(107, 12);
            button3.Name = "button3";
            button3.Size = new Size(75, 23);
            button3.TabIndex = 5;
            button3.Text = "Unselect All";
            button3.UseVisualStyleBackColor = true;
            button3.Click += button3_Click;
            // 
            // newDirButton
            // 
            newDirButton.BackColor = SystemColors.ControlDarkDark;
            newDirButton.FlatAppearance.BorderColor = Color.DarkGray;
            newDirButton.FlatStyle = FlatStyle.Flat;
            newDirButton.Location = new Point(12, 604);
            newDirButton.Name = "newDirButton";
            newDirButton.Size = new Size(145, 23);
            newDirButton.TabIndex = 6;
            newDirButton.Text = "Change Data Directory";
            newDirButton.UseVisualStyleBackColor = false;
            newDirButton.Click += newDirButton_Click;
            // 
            // PluginManagerForm
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            BackColor = Color.DimGray;
            ClientSize = new Size(455, 639);
            Controls.Add(newDirButton);
            Controls.Add(button3);
            Controls.Add(selectAllButton);
            Controls.Add(sortDown);
            Controls.Add(sortUp);
            Controls.Add(pluginsBox);
            Controls.Add(button1);
            FormBorderStyle = FormBorderStyle.SizableToolWindow;
            Icon = (Icon)resources.GetObject("$this.Icon");
            Name = "PluginManagerForm";
            Text = "Starfield Plugin Manager";
            Load += Form1_Load;
            ResumeLayout(false);
        }

        #endregion

        private Button button1;
        private CheckedListBox pluginsBox;
        private Button sortUp;
        private Button sortDown;
        private Button selectAllButton;
        private Button button3;
        private Button justSaveButton;
        private Button newDirButton;
    }
}