﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Unicode;
using System.Threading.Tasks;
using System.Xml;

namespace PluginManager
{
    public class SettingsHandler
    {

        public static List<string> DEFAULT_PLUGINS_LIST = new List<string>(new string[] {
            "Starfield.esm",
            "BlueprintShips-Starfield.esm",
            "Constellation.esm",
            "OldMars.esm"
        });
        public static string SETTINGS_FILE_NAME = "settings.xml";
        public static string DEFAULT_PLUGINS_NAME = "default_plugins.xml";


        public XmlDocument? settingsXml;

        public List<string> bethPlugins { get; } = new List<string>();
        public string defaultPluginLink = "";
        public string starfieldPath = "";
        private string _pluginsTXTPath = "";

        public string pluginsTXTPath {
            get {
                return Environment.ExpandEnvironmentVariables(this._pluginsTXTPath);
            }
            set {
                _pluginsTXTPath= value;
            }
        }


        public void Load() {

            //Load XML if it exists
            if (File.Exists(SETTINGS_FILE_NAME)) {
                this.settingsXml = new XmlDocument();

                using (FileStream s = File.OpenRead(SETTINGS_FILE_NAME)) {
                    this.settingsXml.Load(s);

                    this.readSettings(this.settingsXml);

                }

            }

            downloadOrReadDefaultPlugins();
            this.OpenSelectionIfStarfieldDirInvalid(null);
        }

        public void readSettings(XmlDocument doc) {

            this.starfieldPath = doc.SelectSingleNode("/Settings/StarfieldDirectory")?.InnerText ?? "";

            this.defaultPluginLink = doc.SelectSingleNode("/Settings/DefaultPlugins")?.InnerText ?? "";

            this._pluginsTXTPath = doc.SelectSingleNode("/Settings/PluginsTxtPath")?.InnerText ?? "";

        }

        public async void downloadOrReadDefaultPlugins() {


            if (!String.IsNullOrEmpty(this.defaultPluginLink))
            {
                try
                {
                    HttpClient client = new HttpClient();
                    string? downloadedData = await client.GetAsync(this.defaultPluginLink).Result.Content.ReadAsStringAsync();

                    //Write the data we just downloaded
                    if (!String.IsNullOrEmpty(downloadedData))
                    {
                        using (StreamWriter stream = new StreamWriter(File.Create(DEFAULT_PLUGINS_NAME)))
                        {
                            stream.Write(downloadedData);
                        }
                    }


                }
                catch { }
            }

            this.bethPlugins.Clear();

            //Read the cached file of bethesda plugins
            using (FileStream settingsReader = File.OpenRead(DEFAULT_PLUGINS_NAME)) {
                
                XmlDocument xml = new XmlDocument();
                xml.Load(settingsReader);

                try {
                    var list = xml.SelectNodes("/beth_plugins/plugin[@pluginName]");
                    foreach (XmlNode node in list) {
                        string? defaultPlugin = node.Attributes?.GetNamedItem("pluginName")?.Value ?? null;
                        if (defaultPlugin != null)
                        {
                            this.bethPlugins.Add(defaultPlugin);
                        }
                    }
                }
                catch {
                    
                }

            }

        }

        public bool WriteSettingsFile() {
            if (this.settingsXml == null) {
                return false;
            }

            using (XmlWriter s = XmlWriter.Create(File.Create(SETTINGS_FILE_NAME))) {
                this.settingsXml.WriteContentTo(s);
            }


            return true;
        }

        public void OpenSelectionIfStarfieldDirInvalid(Form? parent, bool force = false)
        {

            if (Directory.Exists(this.starfieldPath) && Directory.GetFiles(this.starfieldPath, "*.esm").Length > 0 && !force) {
                return;
            }

            FolderBrowserDialog browser = new FolderBrowserDialog();
            browser.Description = "Set the Starfield Data folder";
            browser.UseDescriptionForTitle = true;
            DialogResult result = browser.ShowDialog(parent);
            if (result == DialogResult.OK || result == DialogResult.Continue)
            {
                this.starfieldPath = browser.SelectedPath;
                this.settingsXml.SelectSingleNode("/Settings/StarfieldDirectory").InnerText = this.starfieldPath;
                WriteSettingsFile();
            }

        }

    }

    
}
