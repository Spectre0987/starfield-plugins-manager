using System.Collections;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Text;

namespace PluginManager
{
    public partial class PluginManagerForm : Form
    {
        public static List<string> esmLoaded = new List<string>();
        public FileSystemWatcher esmWatcher = new FileSystemWatcher();
        public static int numDefaultPluginsInstalled = -1;

        public SettingsHandler settingsHandler = new SettingsHandler();

        public PluginManagerForm()
        {
            InitializeComponent();
            this.Text = "Starfield Plugin Manager";
            this.settingsHandler.Load();
            numDefaultPluginsInstalled = GetNumberOfDefaults();

            this.SetupWatch(this.settingsHandler.starfieldPath);
        }

        private void SetupWatch(string dataFolder) {
            this.esmWatcher.Path = dataFolder;
            this.esmWatcher.IncludeSubdirectories = false;
            this.esmWatcher.EnableRaisingEvents = true;
            this.esmWatcher.Changed += EsmWatcher_Event;
            this.esmWatcher.Created += EsmWatcher_Event;
            this.esmWatcher.Renamed += EsmWatcher_Rename_Event;
            this.esmWatcher.Deleted += EsmWatcher_Event;
        }

        private void EsmWatcher_Rename_Event(object sender, RenamedEventArgs e) {
            
        }

        private void EsmWatcher_Event(object sender, FileSystemEventArgs e)
        {

            this.Invoke(() => {
                this.SyncGuiWithFile();
            });
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            SyncGuiWithFile();
        }

        private void button1_Click(object sender, EventArgs e)
        {

            writeFile();

            SyncGuiWithFile();

        }

        public void writeFile()
        {

            using (Stream s = File.Open(this.settingsHandler.pluginsTXTPath, FileMode.Create))
            {

                string data = "#\r\n"; // Plugin Enabler mod reqires these lines at the start
                for (int i = 0; i < this.pluginsBox.Items.Count; ++i)
                {
                    string? plugin = (this.pluginsBox.Items[i] as PluginData)?.Name;

                    if (plugin == null || this.settingsHandler.bethPlugins.Contains(plugin))
                        continue;

                    //Add an astrics before the file name if this plugin should be enabled
                    data += ((this.pluginsBox.CheckedIndices.Contains(i) ? "*" : "") + plugin + "\r\n");
                }

                char[] buffer = data.ToCharArray();
                for (int i = 0; i < buffer.Length; ++i)
                {
                    s.WriteByte((byte)buffer[i]);
                }

            }
            MessageBox.Show("Saved plugins file!", "Plugin Status");

        }

        //Move selection either up for down
        public void ModSortSelectd(bool up)
        {
            int selected = this.pluginsBox.SelectedIndex;
            if (selected >= 0)
            {
                bool wasChecked = this.pluginsBox.CheckedIndices.Contains(selected);

                int newId = selected + (up ? -1 : 1);
                if (newId >= 0 && newId < this.pluginsBox.Items.Count)
                {
                    object item = this.pluginsBox.Items[selected];
                    this.pluginsBox.SetSelected(selected, false);
                    this.pluginsBox.Items.RemoveAt(selected);
                    this.pluginsBox.Items.Insert(newId, item);
                    this.pluginsBox.SetSelected(newId, true);
                    this.pluginsBox.SetItemChecked(newId, wasChecked);
                }
            }
        }

        public void SyncGuiWithFile()
        {
            fillPluginList();
            List<string> enabled = GetEnabledPluginsInFile();

            SortBoxes(enabled);
            foreach (string plugin in enabled)
            {
                for (int i = 0; i < this.pluginsBox.Items.Count; ++i)
                {

                    string listItem = (this.pluginsBox.Items[i] as PluginData).Name;

                    if (listItem.Trim().ToLower().Equals(plugin.Trim().ToLower()))
                    { //If enabled plugins has this item
                        this.pluginsBox.SetItemChecked(i, true);
                    }
                }
            }

        }

        public void fillPluginList()
        {
            this.pluginsBox.Items.Clear();

            List<string> downloadedPlugins = new List<string>(Directory.GetFiles(this.settingsHandler.starfieldPath, "*.esm", SearchOption.TopDirectoryOnly));
            downloadedPlugins.AddRange(Directory.GetFiles(this.settingsHandler.starfieldPath, "*.esp", SearchOption.TopDirectoryOnly));


            foreach (string esm in downloadedPlugins)
            {
                string baseFile = Path.GetFileName(esm).Trim();
                bool isDefault = false;
                for (int i = 0; i < this.settingsHandler.bethPlugins.Count; ++i) {
                    if (baseFile.Equals(this.settingsHandler.bethPlugins[i])) {
                        isDefault = true;
                        break;
                    }
                }

                if(!isDefault)
                    this.pluginsBox.Items.Add(new PluginData(null, baseFile), false);
            }
        }

        public void SortBoxes(List<string> pluginFile)
        {
            for (int i = pluginFile.Count - 1; i >= 0; i--)
            {
                string plugin = pluginFile[i];
                //Find Matching Plugin
                IEnumerator pluginEnumerator = this.pluginsBox.Items.GetEnumerator();
                while (pluginEnumerator.MoveNext()) {
                    PluginData? pluginName = pluginEnumerator.Current as PluginData;

                    if (pluginName != null && plugin.Equals(pluginName.Name)) {
                        this.pluginsBox.Items.Remove(pluginName);
                        this.pluginsBox.Items.Insert(0, pluginName.SetLoad(i));
                        break;
                    }

                }
            }
        }

        public List<string> GetEnabledPluginsInFile()
        {
            List<string> plugins = new List<string>();

            using (Stream s = File.Open(this.settingsHandler.pluginsTXTPath, FileMode.OpenOrCreate))
            {

                string data = "";
                int len = 0;
                byte[] buffer = new byte[1024];

                while ((len = s.Read(buffer, 0, 1024)) != 0)
                {
                    for (int i = 0; i < len; ++i)
                    {
                        data += (char)buffer[i];
                    }
                }

                data = data.Trim();
                string[] lines = data.Split("\r\n");
                foreach (string plugin in lines)
                {
                    if (plugin.StartsWith("*"))
                    {

                        plugins.Add(plugin.Substring(1));

                    }
                }

            }

            return plugins;
        }

        private void sortUp_Click(object sender, EventArgs e)
        {
            ModSortSelectd(true);
        }

        private void sortDown_Click(object sender, EventArgs e)
        {
            ModSortSelectd(false);
        }

        private void selectAllButton_Click(object sender, EventArgs e)
        {
            SetAll(true);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            SetAll(false);
        }

        //Select or deselect all plugins
        public void SetAll(bool selected)
        {

            for (int i = 0; i < this.pluginsBox.Items.Count; ++i)
            {
                this.pluginsBox.SetItemChecked(i, selected);
            }
        }

        private void newDirButton_Click(object sender, EventArgs e)
        {
            this.settingsHandler.OpenSelectionIfStarfieldDirInvalid(this, true);
            this.SyncGuiWithFile();

        }

        public static string CalculateLoadOrder(int num) {
            string order = String.Format("{0:X}", num + numDefaultPluginsInstalled);
            return (order.Length < 2 ? "0" : "") + order;
        }

        public int GetNumberOfDefaults() {

            int numberOfInstalled = 0;

            foreach (string path in Directory.GetFiles(this.settingsHandler.starfieldPath)) {
                foreach (string plugin in this.settingsHandler.bethPlugins) {
                    if (plugin.ToLower().Equals(Path.GetFileName(path).ToLower())) {
                        numberOfInstalled++;
                    }
                }
            }

            return numberOfInstalled;

        }
    }

    class PluginData{

        public string? LoadOrder { get; set; }
        public string Name { get; }

        public PluginData(string? loadOrder, string name) {
            this.LoadOrder = loadOrder;
            this.Name = name;
        }

        public PluginData SetLoad(int index) {
            this.LoadOrder = PluginManagerForm.CalculateLoadOrder(index);
            return this;
        }

        public override string ToString()
        {
            return (this.LoadOrder != null ? (LoadOrder + " ") : "") + Name.ToString();
        }

    }
}